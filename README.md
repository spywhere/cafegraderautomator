## CafeGrader Automation tools

This document use following notations...

`<>` indicated required<br>
`[]` indicated optional<br>
`</>` indicated "one of"<br>
`<...>` indicated a one or more<br>
`%` indicated a number

### Command Usages
`python cga.py [command] [project_file]`

when

- `command` is either `new` or `edit` to create a new project or edit an existing project.
- `project_file` is a relative path to project file. If project file is not found, a new one will be created instead.

After you specified a proper command, you will be able to use internal commands when you see the program prompting `:` (see `Internal Usages` below).

### Internal Usages

`<command> <contest name ...>`

or

`schedule <datetime format> <command> <contest name ...>`

when

- `command` is one of `update`, `enable`, `disable` or `remove` (See each command description belows).
- `contest name ...` is a list of string (with proper command escape) indicated contest names to be used with specified command.
- `date/time` is a string representation of date and/or time (See `Date/Time format` belows)
- For `schedule ...` command, see `Schedule Tasks` for more informations

### Update command
Update command will fetch and replace all problems within specified contest from grader into local project.

### Enable command
Enable command will enable problems within specified contests.

### Disable command
Disable command will remove specified contests from associated problems.

### Remove command
Remove command will remove specified contests from local project. This command, however, **does not** remove/disable any contest or problem in grader.

### Date/Time format
Date/Time is a string represents a specified time or date. This can be varies as follows:

##### `%h%m%s`
This will represents a timestamp calculated by adding current timestamp with specified duration.

You can reduced the format to any combination of `%h`, `%m` and `%s` but these still required the order to be `%h` follows by `%m` and follows by `%s`

##### `%/%/% %:%:%`
The first part (`%/%/%`) will represents a date/month/year which can be reduced to just `%/%` to indicated a date/month with current year. If date is not specified, current date will be used as default.

The second part (`%:%:%`) will represents a time (`hour:minute:second`) of the day which can be reducted to just `%:%` to represents as `hour:minute`. If time is not specified, `00:00:00` will be used as default.

In a varies of date you can used following words to indicated a frequently used date...

- `today` to represents current date
- `tomorrow` or `next [day]` to represents the day after current date
- `next week` to represents the day after one week (7 days) from current date
- `next <sunday/monday/tuesday/wednesday/thursday/friday/saturday>` to represents the next weekday from current date (exclusive)
- `next <sun/mon/tue/wed/thu/fri/sat>` to represents the same as above but in a short form

And frequently used time...

- `morning` to represents `06:00:00`
- `class` or `office` to represents `09:00:00`
- `lunch` or `midday` to represents `12:00:00`
- `afternoon` to represents `13:00:00`
- `late` to represents `16:00:00`

### Schedule Tasks
Schedule task will run specified command on specified date/time.

To view currently assigned tasks run

`schedule list` or `schedule`

To remove tasks run

`schedule remove [schedule id ...]`

----

**Developed by:** Sirisak Lueangsaksri<br>
**Contributed to:**<br>
Department of Computer Engineering<br>
Faculty of Engineering at Sri Racha<br>
Kasetsart University, Sri Racha Campus.