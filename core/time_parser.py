import re
import time

TRIM_FORMAT = re.compile("^\\s+|\\s+$")
DURATION_FORMAT = re.compile("^((\\d+)h)?((\\d+)m)?((\\d+)s)?$", re.I)
DATE_FORMAT = re.compile("((\\d{1,2})/?(\\d{1,2})?/?(\\d{2,4})?)|((today|tomorrow|next)( (day|week|sun|mon|tue|wed|thu|fri|sat))?)", re.I)
# 1 for date format
# 2 for date
# 3 for month
# 4 for year
# 5 for date string
# 6 for first word
# 8 for word after "next" (use startswith)
TIME_FORMAT = re.compile("((\\d{1,2}):?(\\d{1,2})?:?(\\d{1,2})?)|(morning|class|office|lunch|midday|afternoon|late)", re.I)
# 1 for time format
# 2 for hour
# 3 for minute
# 4 for sec
# 5 for time string
DAY_NAMES = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
MONTH_NAMES = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]


def trim(string):
    return TRIM_FORMAT.sub("", string)


def time_string(timestamp):
    localtime = time.localtime(timestamp)
    return "%s %d %s %0.4d %0.2d:%0.2d:%0.2d" % (DAY_NAMES[localtime.tm_wday], localtime.tm_mday, MONTH_NAMES[localtime.tm_mon], localtime.tm_year, localtime.tm_hour, localtime.tm_min, localtime.tm_sec)


def parse_time(time_format):
    time_format = trim(time_format)
    datetimestr = time_format.split(" ")

    localtime = time.localtime()
    cdate = time.mktime((localtime.tm_year, localtime.tm_mon, localtime.tm_mday, 0, 0, 0, localtime.tm_wday, localtime.tm_yday, localtime.tm_isdst))  # Get today midnight timestamp
    ctime = time.mktime(localtime) - cdate

    date_match = None
    time_match = None
    if len(datetimestr) > 0:
        duration_match = DURATION_FORMAT.match(time_format)
        if duration_match is None:
            if len(datetimestr) > 1:
                time_match = TIME_FORMAT.match(datetimestr[-1])
                if time_match is not None:
                    date_match = DATE_FORMAT.match(" ".join(datetimestr[:-1]))
                else:
                    date_match = DATE_FORMAT.match(time_format)
            else:
                date_match = DATE_FORMAT.match(time_format)
        else:
            duration = 0
            if duration_match.group(2) is not None:
                duration += int(duration_match.group(2))*3600
            if duration_match.group(4) is not None:
                duration += int(duration_match.group(4))*60
            if duration_match.group(6) is not None:
                duration += int(duration_match.group(6))
            return time.mktime(time.localtime())+duration

    if date_match is not None:
        if date_match.group(1) is not None:
            date = localtime.tm_mday
            month = localtime.tm_mon
            year = localtime.tm_year

            if date_match.group(2) is not None:
                date = int(date_match.group(2))
            if date_match.group(3) is not None:
                month = int(date_match.group(3))
            if date_match.group(4) is not None:
                year = int(date_match.group(4))
                if year < 100:
                    year += 2000

            localtime = time.strptime("%s %s %s" % (date, month, year), "%d %m %Y")
            cdate = time.mktime((localtime.tm_year, localtime.tm_mon, localtime.tm_mday, 0, 0, 0, localtime.tm_wday, localtime.tm_yday, localtime.tm_isdst))  # Get today midnight timestamp
            ctime = time.mktime(localtime) - cdate
        elif date_match.group(5) is not None:
            first_word = date_match.group(6)
            next_word = date_match.group(8)
            if first_word is not None:
                first_word = first_word.lower()
                if first_word == "tomorrow" or (first_word == "next" and (next_word is None or next_word == "day")):
                    cdate += 86400
                elif first_word == "next" and next_word is not None:
                    next_word = next_word.lower()
                    day_index = is_day_name(next_word)

                    if next_word == "week":
                        cdate += 604800
                    elif day_index >= 0:
                        remaining_days = day_index-localtime.tm_wday
                        if remaining_days <= 0:
                            remaining_days += 7
                        cdate += remaining_days*86400
    if time_match is not None:
        if time_match.group(1) is not None:
            hour = localtime.tm_hour
            minute = localtime.tm_min
            second = localtime.tm_sec

            if time_match.group(2) is not None:
                hour = int(time_match.group(2))
            elif time_match.group(3) is not None:
                minute = int(time_match.group(3))
            elif time_match.group(4) is not None:
                second = int(time_match.group(4))
            ctime = hour*3600+minute*60+second
        elif time_match.group(5) is not None:
            time_word = time_match.group(5).lower()
            if time_word == "morning":
                ctime = 21600
            elif time_word == "class" or time_word == "office":
                ctime = 32400
            elif time_word == "lunch" or time_word == "midday":
                ctime = 43200
            elif time_word == "afternoon":
                ctime = 46800
            elif time_word == "late":
                ctime = 57600

    return cdate+ctime


def is_day_name(name):
    index = 0
    for day in [x.lower() for x in DAY_NAMES]:
        if day.startswith(name) and len(day) >= len(name):
            return index
        index += 1
    return -1
