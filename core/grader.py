import mechanize
import re


GRADER_URL = "http://grader.eng.src.ku.ac.th"


class grader:
    def _check_grader(self, handler=None):
        br = mechanize.Browser()
        br.set_handle_robots(False)
        try:
            br.open(GRADER_URL)
            return br
        except Exception as msg:
            if handler is not None:
                handler("Grader Error: %s" % (str(msg)))
        return None

    def validate_account(self, user, password, logger=None, handler=None):
        if logger is not None:
            logger("Connecting to grader...")
        br = self._check_grader()
        if br is None:
            return False
        if len(list(br.forms())) < 1:
            if handler is not None:
                handler("Grader Error: No login form in grader")
            return False
        br.form = list(br.forms())[0]
        br.form["login"] = user
        br.form["password"] = password
        if logger is not None:
            logger("Logging into grader...")
        if re.search("Wrong password", br.submit().read()) is not None:
            return False
        return True
