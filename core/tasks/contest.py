from core import task


class contest(task):
    @classmethod
    def task_type(cls):
        return "contest"

    @classmethod
    def is_task(cls, data):
        return False

    @classmethod
    def new(cls):
        return cls()

    @classmethod
    def load(cls, data):
        return cls()

    def edit_task(self):
        pass

    def is_valid_command(self, command):
        return False

    def run(self, grader, google, command):
        print str(command)
        pass

    def description(self):
        return "A contest task"
