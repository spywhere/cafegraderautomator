class task:
    schedules = []

    @classmethod
    def task_types(cls):
        from tasks import contest, report
        return [contest.task_type(), report.task_type()]

    @classmethod
    def new_task(cls, task_type):
        from tasks import contest, report
        if report.task_type() == task_type:
            return report.new()
        elif contest.task_type() == task_type:
            return contest.new()
        return None

    @classmethod
    def load_task(cls, data):
        from tasks import contest, report
        if report.is_task(data):
            return report.load(data)
        elif contest.is_task(data):
            return contest.load(data)
        return None

    @classmethod
    def task_type():
        return ""

    @classmethod
    def is_task(cls, data):
        return False

    @classmethod
    def new(cls):
        return cls()

    @classmethod
    def load(cls, data):
        return cls()

    def schedules(self):
        if len(self.schedules) <= 0:
            print "   No schedule"
        else:
            print "%s schedule(s):" % (len(self.schedules))
            index = 1
            for schedule in self.schedules:
                print "%s. %s %s" % (schedule.readableTime(), " ".join(schedule.commands()))
                index += 1

    def edit_task(self):
        pass

    def is_valid_command(self, command):
        return False

    def run(self, grader, google, command):
        pass

    def description(self):
        return "An empty task"
