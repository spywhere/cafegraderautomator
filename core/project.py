import getpass
import core
import os.path


GOOGLE_APP_PASSWORD_URL = "https://security.google.com/settings/security/apppasswords"


class project:
    grader_user = None
    grader_password = None
    contest = {}

    def __init__(self, filename):
        self.project_file = filename
        self.grader = core.grader()

    def simple_printer(self, text):
        print text

    def can_close(self):
        return True

    def close(self):
        if self.can_close():
            return True
        print "Project is still running in the background"
        while True:
            confirm = raw_input("Do you want to close this project now? (y/n): ")
            if confirm.lower() == "y":
                return True
            elif confirm != "":
                return False

    def open(self):
        if not os.path.exists(self.project_file):
            return None
        f = open(self.project_file, "r")
        if f is None:
            print "Error occured while reading a file"
            return None
        data = f.read().split("\n")
        if len(data) < 2:
            print "Invalid file structure"
            return None
        self.grader_user = data[0]
        data = data[1:]
        self.grader_password = data[0]
        data = data[1:]

        return self

    def save(self):
        f = open(self.project_file, "w")
        data = []
        data.append(self.grader_user)
        data.append(self.grader_password)
        f.write(str.join("\n", data))
        f.close()

    def edit(self, new=False):
        print "==== %s Project [%s] ====" % ("New" if new else "Edit", self.project_file)
        if not new:
            print "Leave field empty to keep current settings"
        print "==== Grader Account ===="
        while True:
            grader_user = raw_input("Grader User: ")
            if grader_user == "":
                if self.grader_user is None:
                    return None
            else:
                self.grader_user = grader_user
            grader_password = getpass.getpass("Grader Password: ")
            if grader_password == "":
                if self.grader_password is None:
                    return None
            else:
                self.grader_password = grader_password
            print "Validating..."
            if self.grader.validate_account(self.grader_user, self.grader_password, handler=self.simple_printer):
                break
            else:
                print "Invalid User/Password"

        print "==== Summary ===="
        print "Grader User: %s" % (self.grader_user)
        print "Grader Password: %s" % ("*" * len(self.grader_password))
        print "========"
        while True:
            confirm = raw_input("Confirm? (y/n): ")
            if confirm.lower() == "y":
                self.save()
                break
            elif confirm != "":
                break
        return self

    def get_task(self, name):
        return self.tasks[name] if name in self.tasks else None

    def run_task(self, name, command):
        task = self.get_task(name)
        if task is None:
            return
        task.run(self.grader, self.google, command)

    def schedule_task(self, name):
        task = self.get_task(name)
        if task is None:
            return
        task.schedules()

    def edit_task(self, name, new=False):
        print "==== %s Task [%s] ====" % ("New" if new else "Edit", name)
        if not new:
            print "Leave field empty to keep current settings"
        task_types = core.task.task_types()
        while True:
            task_type = raw_input("Task [%s]: " % ("/".join(task_types)))
            if task_type == "":
                if new:
                    return None
                else:
                    break
            if task_type in task_types:
                break
            else:
                print "Invalid task type"
        if new:
            task = core.task.new_task(task_type)
        else:
            self.get_task(name).edit_task()
        if task is not None:
            self.tasks[name] = task
        return task

    def remove_task(self, name):
        if self.get_task(name) is None:
            return
        del self.tasks[name]

    def list_tasks(self):
        if len(self.tasks) <= 0:
            print "   No task"
        else:
            print "%s task(s):" % (len(self.tasks))
            for task in self.tasks:
                print "   [%s] %s" % (task, self.tasks[task].description())


def new_project(filename):
    return project(filename).edit(new=True)


def open_project(filename):
    return project(filename).open()


def edit_project(filename):
    proj = open_project(filename)
    return proj.edit() if proj is not None else None
