import gspread


class google:
    def validate_account(self, gmail, password, logger=None, handler=None):
        if logger is not None:
            logger("Logging into GMail...")
        try:
            return gspread.login(gmail, password)
        except gspread.AuthenticationError:
            return None
        except Exception as e:
            handler("Google Error: %s" % (str(e)))
        return None
