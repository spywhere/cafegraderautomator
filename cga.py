# import bs4
# import gspread
# import mechanize
import core
import argparse
import shlex
import os
import traceback
try:
    import readline
    readline
except ImportError:
    pass

"""
python cga.py

update/remove/enable/disable [contest name ...]
schedule date/time update/remove/enable/disable [contest name ...]

RegEx "^pattern.*" as default
Add ! to override to "pattern"
Add @ to override to normal string
Add . to search from starts as normal string

"""

'''
D/M/Y H:M:S
(DataType):(Data)
   ContestData 1:(Contest):(Problem),(Problem),...
   ScheduleData 2:(OpenTimestamp):(CloseTimestamp)
'''

'''
Generate GoogleDrive report
Automated open/close problems based on contests
'''


def run():
    parser = argparse.ArgumentParser(description="CafeGrader automation tools")
    parser.add_argument("command", nargs="?", choices=["new", "edit"], help="action to perform")
    parser.add_argument("project_file", nargs="?", default="config", type=str, help="project file to use")
    options, remain = parser.parse_known_args()
    if options.command is not None:
        if options.command == "new":
            project = core.new_project(options.project_file)
        elif options.command == "edit":
            project = core.edit_project(options.project_file)
    else:
        project = core.open_project(options.project_file)
        if project is None:
            project = core.new_project(options.project_file)

    print "==== Project [%s] ====" % (options.project_file)
    print "Enter \"?\" or \"-h\" or \"--help\" for help or nothing to exit"
    while True:
        try:
            schedule_restricted_commands = ["remove", "clear", "system", "schedule"]
            command_list = ["update", "enable", "disable", "remove", "clear", "system", "schedule"]

            raw_command = raw_input(": ")
            schedule_time = None
            if core.trim(raw_command).lower().startswith("system"):
                command_comp = core.trim(raw_command).lower().split(" ")[1:]
                os.system(" ".join(command_comp))
            if core.trim(raw_command).lower().startswith("schedule"):
                command_comp = core.trim(raw_command).lower().split(" ")[1:]
                if len(command_comp) == 0 or command_comp[0] == "list":
                    print "List schedules"
                    continue
                elif command_comp[0] == "remove":
                    command_comp = command_comp[1:]
                    for schedulestr in command_comp:
                        try:
                            schedule = int(schedulestr)
                            print "Remove schedule %s" % (schedule)
                        except Exception:
                            print "Invalid schedule %s" % (schedulestr)
                    continue
                datetime = None
                raw_command = None
                index = 0
                for cmd in command_comp:
                    if cmd in command_list:
                        break
                    index += 1
                datetime = " ".join(command_comp[:index])
                raw_command = " ".join(command_comp[index:])
                schedule_time = core.parse_time(datetime)
                print "Schedule tasks on %s" % (core.time_string(schedule_time))
            commands = shlex.split(raw_command)
            if commands and schedule_time is not None:
                if [cmd for cmd in commands if cmd in schedule_restricted_commands]:
                    print "Command %s is not available in schedule mode" % (commands[0])
                    continue
            # parser = argparse.ArgumentParser(prog="", add_help=False, prefix_chars=":")
            # parser.add_argument("command", nargs="?", choices=["schedule", "new", "edit", "del", "rm", "ls"], help="command to perform")
            # parser.add_argument("task", type=str, help="task to perform")
            if len(commands) <= 0:
                if project.can_close() or project.close():
                    break
                else:
                    continue

            # task_options, remain = parser.parse_known_args(shlex.split(command))
            # command = None
            # task_name = None
            # task = None
            # if len(commands) > 0 and commands[0] in ["schedule", "new", "edit", "del", "rm", "ls", "?", "-h", "--help"]:
            #     command = commands[0]
            #     commands = commands[1:]
            # if len(commands) > 0:
            #     task_name = commands[0]
            #     task = project.get_task(task_name)
            #     commands = commands[1:]
            # if command is not None:
            #     if command == "ls":
            #         project.list_tasks()
            #     elif command == "?" or command == "-h" or command == "--help":
            #         print "Usage: main_command task_name"
            #         print "   or: sub_command"
            #         print "   or: task_name [arg...]"
            #         print ""
            #         print "Main Commands:"
            #         print "   new\t\tCreate a new task"
            #         print "   schedule\tManage a schedule on specified task"
            #         print "   edit\t\tEdit existing task"
            #         print "   del, rm\tRemove existing task"
            #         print ""
            #         print "Sub Commands:"
            #         print "   ls\t\tShow all tasks"
            #     elif task_name is None:
            #         print "usage: %s <task name>" % (command)
            #     elif command == "new":
            #         project.edit_task(task_name, new=True)
            #     elif task is None:
            #         print "Task \"%s\" is not found" % (task_name)
            #     elif command == "schedule":
            #         project.schedule_task(task_name)
            #     elif command == "edit":
            #         project.edit_task(task_name, new=False)
            #     elif command == "del" or command == "rm":
            #         project.remove_task(task_name)
            # elif task is not None:
            #     task.run(commands)
            # else:
            #     print "Invalid command"
            #     print "Enter \"?\" or \"-h\" or \"--help\" for help"
            # if task_options.command is not None:
            #     if task_options.command == "new":
            #         project.edit_task(task_options.task, new=True)
            #     elif task_options.command == "edit":
            #         project.edit_task(task_options.task, new=False)
            #     elif task_options.command == "del" or task_options.command == "rm":
            #         project.remove_task(task_options.task)
            #     elif task_options.command == "ls":
            #         project.list_tasks()
            # elif task_options.task is not None:
            #     task = project.get_task(task_options.task)
            #     if task_options.task == "ls":
            #         project.list_tasks()
            #     elif task_options.task in ["?", "-h", "--help"]:
            #         parser.print_help()
            #     elif task is not None:
            #         task.run(remain)
            #     else:
            #         print "Task \"%s\" is not found" % (task_options.task)
            # else:
            #     print "Invalid command"
            #     parser.print_help()
        except SystemExit:
            continue


if __name__ == "__main__":
    run()
